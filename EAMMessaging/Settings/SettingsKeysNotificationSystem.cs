﻿using Xafari.BC.Settings;

namespace PIR.EAM.Messaging.Settings
{  
    /// <summary>Класс содержит метод, предоставляющий доступ к 
    /// дополнительным (расширенным) узлам модели настроек.
    /// </summary>
    public static class SettingsKeys
    {
        /// <summary>Метод предоставляет доступ к дополнительным узлам модели настроек
        /// </summary>
        /// <param name="iModelBCSettings"></param>
        /// <returns></returns>
        public static INotificationSystemExtendedSettings NotificationSystemSettingsEx(this IModelBCSettings iModelBCSettings)
        {
            return (INotificationSystemExtendedSettings)iModelBCSettings;
        }
    }   
}
