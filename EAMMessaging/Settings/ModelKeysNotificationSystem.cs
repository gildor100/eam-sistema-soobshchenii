﻿using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using Galaktika.Core.Branches;
using Galaktika.Core.Module;
using PIR.EAM.Services.Settings.BusinessObjects;
using System.ComponentModel;
using Xafari.BC.Settings;

namespace PIR.EAM.Messaging.Settings
{  
    #region Корневой элемент добавляемого узла настроек

    /// <summary>Корневой интерфейс новой ветки настроек
    /// </summary>
    public interface INotificationSystemExtendedSettings
    {
        INotificationSystem NotificationSystem { get; }
    }
    
    #endregion    

    #region Группы настроек

    /// <summary>Элемент узла настроек "Система уведомлений"
    /// </summary>
    public interface INotificationSystem : IModelBCSettingsGroup
    {
        IPopupMessagesGroup PopupMessages { get; }
    }

    /// <summary>Элемент узла настроек "Всплывающие сообщения".
    /// </summary>
    public interface IPopupMessagesGroup : IModelBCSettingsGroup
    {
        IExcludeObjectTypesGroup ExcludeObjectTypes { get; }

        INotificationSoundGroup NotificationSound { get; }
        IModelKeyNotificationBehavior NotificationBehavior { get; }
    }

    /// <summary>Элемент узла настроек "Настройки звука".
    /// </summary>
    public interface INotificationSoundGroup : IModelBCSettingsGroup
    {
        IModelKeyPlayNotificationSound PlayNotificationSound { get; }

        IModelKeyPathToSoundFile PathToSoundFile { get; }
    }

    public interface IModelKeyNotificationBehavior : IModelBCSettingsGroup
    {
        IModelKeyNotificationAllowFastClose AllowFastCloseOther { get; }
        IModelKeyNotificationAllowFastClose AllowFastCloseIncedentCreate { get; }
        IModelKeyNotificationAllowFastClose AllowFastCloseChallengeReject { get; }
        IModelKeyNotificationAllowFastClose AllowFastCloseChallengeRework { get; }
        IModelKeyNotificationAllowFastClose AllowFastCloseChallengeInWorkFromReject { get; }
        IModelKeyNotificationAllowFastClose AllowFastCloseOrdinanceCreate { get; }
        IModelKeyNotificationAllowFastClose AllowFastCloseOrdinanceAllClose { get; }
    }
    

    /// <summary>Элемент узла настроек "Особое поведение для объектов" 
    /// Группа настроек-исключений, содержащих типы объектов, для которых при щелчке 
    /// на всплывающее окно сообщения нужно показывать
    /// представление (View) самого объекта, связанного с этим сообщением, 
    /// а не представление самого сообщения 
    /// </summary>
    public interface IExcludeObjectTypesGroup : IModelBCSettingsGroup
    {
        IModelKeyTypeStorageValue01 TypeStorageValue01 { get; }

        IModelKeyTypeStorageValue02 TypeStorageValue02 { get; }

        IModelKeyTypeStorageValue03 TypeStorageValue03 { get; }

        IModelKeyTypeStorageValue04 TypeStorageValue04 { get; }

        IModelKeyTypeStorageValue05 TypeStorageValue05 { get; }

        IModelKeyTypeStorageValue06 TypeStorageValue06 { get; }

        IModelKeyTypeStorageValue07 TypeStorageValue07 { get; }

        IModelKeyTypeStorageValue08 TypeStorageValue08 { get; }

        IModelKeyTypeStorageValue09 TypeStorageValue09 { get; }

        IModelKeyTypeStorageValue10 TypeStorageValue10 { get; }  
        
        /// <summary>Фиктивная настройка для обхода ошибки, описанной здесь: 
        /// http://xafari.ru/news/osobennosti-opisanie-nastrojki-persistentnogo-tipa.html
        /// </summary>
        [Browsable(false)]
        БазовыйОбъект FictitiousBaseObject { get; }

        /// <summary>Фиктивная настройка для обхода ошибки, описанной здесь: 
        /// http://xafari.ru/news/osobennosti-opisanie-nastrojki-persistentnogo-tipa.html
        /// </summary>
        [Browsable(false)]
        ISupportBranch FictitiousSupportBranch { get; }   
    }
    
    #endregion

    #region Настройки
    public interface IModelKeyNotificationAllowFastClose : IModelBCSettingsItem<bool> { }
    
    /// <summary>Настройка "Тип объекта 01". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue01 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Тип объекта 02". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue02 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Тип объекта 03". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue03 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Тип объекта 04". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue04 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Тип объекта 05". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue05 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Тип объекта 06". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue06 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Тип объекта 07". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue07 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Тип объекта 08". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue08 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Тип объекта 09". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue09 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Тип объекта 10". Хранит объект типа "SettingsExTypeStorage"
    /// </summary>
    public interface IModelKeyTypeStorageValue10 : IModelBCSettingsObjectItem<SettingsExTypeStorage>, IModelNode { }

    /// <summary>Настройка "Проигрывать звук при входящем уведомлении". Тип: bool
    /// </summary>
    public interface IModelKeyPlayNotificationSound : IModelBCSettingsItem<bool>, IModelNode { }
     
    /// <summary>Настройка "Путь к звуковому файлу". Тип: string
    /// </summary>
    public interface IModelKeyPathToSoundFile : IModelBCSettingsItem<string>, IModelNode { }

    #endregion

    #region Доменная логика

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue01))]
    public class IModelKeyTypeStorageValue01Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue01>
    {
        public IModelKeyTypeStorageValue01Logic(IModelKeyTypeStorageValue01 instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue02))]
    public class IModelKeyTypeStorageValue02Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue02>
    {
        public IModelKeyTypeStorageValue02Logic(IModelKeyTypeStorageValue02 instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue03))]
    public class IModelKeyTypeStorageValue03Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue03>
    {
        public IModelKeyTypeStorageValue03Logic(IModelKeyTypeStorageValue03 instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue04))]
    public class IModelKeyTypeStorageValue04Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue04>
    {
        public IModelKeyTypeStorageValue04Logic(IModelKeyTypeStorageValue04 instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue05))]
    public class IModelKeyTypeStorageValue05Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue05>
    {
        public IModelKeyTypeStorageValue05Logic(IModelKeyTypeStorageValue05 instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue06))]
    public class IModelKeyTypeStorageValue06Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue06>
    {
        public IModelKeyTypeStorageValue06Logic(IModelKeyTypeStorageValue06 instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue07))]
    public class IModelKeyTypeStorageValue07Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue07>
    {
        public IModelKeyTypeStorageValue07Logic(IModelKeyTypeStorageValue07 instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue08))]
    public class IModelKeyTypeStorageValue08Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue08>
    {
        public IModelKeyTypeStorageValue08Logic(IModelKeyTypeStorageValue08 instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue09))]
    public class IModelKeyTypeStorageValue09Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue09>
    {
        public IModelKeyTypeStorageValue09Logic(IModelKeyTypeStorageValue09 instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyTypeStorageValue10))]
    public class IModelKeyTypeStorageValue10Logic : IModelBCSettingsXPObjectItemLogic<SettingsExTypeStorage, IModelKeyTypeStorageValue10>
    {
        public IModelKeyTypeStorageValue10Logic(IModelKeyTypeStorageValue10 instance) : base(instance) { }
    } 

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyPlayNotificationSound))]
    public class IModelKeyPlayNoticeSoundLogic : IModelBCSettingsItemLogic<bool, IModelKeyPlayNotificationSound>
    {
        public IModelKeyPlayNoticeSoundLogic(IModelKeyPlayNotificationSound instance) : base(instance) { }
    }

    /// <summary>Доменная логика класса
    /// </summary>
    [DomainLogic(typeof(IModelKeyPathToSoundFile))]
    public class IModelKeyPathToSoundFileLogic : IModelBCSettingsItemLogic<string, IModelKeyPathToSoundFile>
    {
        public IModelKeyPathToSoundFileLogic(IModelKeyPathToSoundFile instance) : base(instance) { }
    }
    [DomainLogic(typeof(IModelKeyNotificationAllowFastClose))]
    public class IModelKeyNotificationAllowFastCloseLogic : IModelBCSettingsItemLogic<bool, IModelKeyNotificationAllowFastClose>
    {
        public IModelKeyNotificationAllowFastCloseLogic(IModelKeyNotificationAllowFastClose instance) : base(instance) { }
    }
    #endregion
}
