﻿using DevExpress.ExpressApp;
using PIR.EAM.Messaging.BusinessObjects;
using System;

namespace PIR.EAM.Messaging.Controllers
{
    public class MessageDetailViewController : ViewController<DetailView>
    {
        public MessageDetailViewController()
        {
            TargetObjectType = typeof (Message);
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            View.CurrentObjectChanged += CurrentObjectChanged;
            var message = View.CurrentObject as Message;
            if (message != null)
            {
                message.Delivered = true;
                message.Save();
                ObjectSpace.CommitChanges();
            }
        }

        private void CurrentObjectChanged(object sender, EventArgs e)
        {
            var message = View.CurrentObject as Message;
            if (message != null)
            {
                message.Delivered = true;
                message.Save();
                ObjectSpace.CommitChanges();
            }
        }
    }
}
