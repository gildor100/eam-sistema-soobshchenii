﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.XtraBars.Alerter;
using PIR.EAM.Messaging.Enums;
using PIR.EAM.Messaging.Properties;
using PIR.EAM.Messaging.Settings;
using PIR.EAM.Services.Settings.BusinessObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Windows.Forms;
using Xafari.BC.Settings;
using Message = PIR.EAM.Messaging.BusinessObjects.Message;

namespace PIR.EAM.Messaging.Controllers
{
    /// <summary>Контроллер уведомлений, реализует сканирование базы на предмет непрочитанных уведомлений
    /// и вывод уведомлений пользователю
    /// </summary>
    public class MessageController : WindowController
    {
        private SoundPlayer _player;
        private IObjectSpace _objectSpace;
        private CollectionSource _collection;

        public AlertControl AlertControl { get; private set; }
        public Timer AlertTimer { get; private set; }
        public IList<Message> Messages { get; private set; }

        public MessageController()
        {
            TargetWindowType = WindowType.Main;
        }

        /// <summary>Активация, инициализируем AlertControl и таймер
        /// </summary>
        protected override void OnActivated()
        {
            base.OnActivated();
            InitAlertControlCore();
            InitAlertTimerCore();

            _player = new SoundPlayer();
        }

        /// <summary>Деактивация, останавливаем таймер
        /// </summary>
        protected override void OnDeactivated()
        {
            if (AlertTimer != null) AlertTimer.Stop();
            base.OnDeactivated();
        }

        /// <summary>Создаем AlertControl и вешаем на него обработчик нажатия
        /// </summary>
        protected virtual void InitAlertControlCore()
        {
            AlertControl = new AlertControl
            {
                AllowHotTrack = true,
                //AllowHotTrack = false,
                AllowHtmlText = true,
                AutoHeight = true,
                AutoFormDelay = 30000,
                FormLocation = AlertFormLocation.BottomRight,
                FormDisplaySpeed = AlertFormDisplaySpeed.Fast,
                ControlBoxPosition = AlertFormControlBoxPosition.Right,
                FormShowingEffect = AlertFormShowingEffect.FadeIn,
                FormMaxCount = 1
            };

            var ab = new AlertButton
            {
                Name = "ShowAll",
                Image = Resources.view12,
                Style = AlertButtonStyle.Button,//AlertButtonStyle.CheckButton,
                Hint = "Показать/скрыть все уведомления"
            };

            AlertControl.Buttons.Add(ab);
            AlertControl.ButtonClick += AlertControlButtonClick;
            AlertControl.AlertClick += AlertControlAlertClick;
            AlertControl.FormClosing += AlertControlFormClosing;
        }

        /// <summary>При закрытии окна уведомления пользователем помечать его как доставленное если
        /// соответствующий флаг в сообщении установлен
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AlertControlFormClosing(object sender, AlertFormClosingEventArgs e)
        {
            if (e.CloseReason == AlertFormCloseReason.UserClosing)
            {
                // Исправление бага, возникающего когда: 
                // - алерт всплывает
                // - после происходит смена пользователя
                // - юзер нажимает на крестик в углу алерта
                // В этот момент Application равен null. 
                if (Application == null) return;

                var objectSpace = Application.CreateObjectSpace();
                var message = objectSpace.GetObject(e.AlertForm.Info.Tag) as Message;
                if (message != null)
                {
                    var slice = SettingsAccessor.Instance.GetSlice(SecuritySystem.CurrentUser);
                    var messageSettings = SettingsAccessor.Instance
                        .GetRootSettings(slice)
                        .NotificationSystemSettingsEx()
                        .NotificationSystem
                        .PopupMessages
                        .NotificationBehavior;
                    if (
                        (message.MessageKind == MessageKind.Other && 
                            messageSettings.AllowFastCloseOther.Value) ||
                        (message.MessageKind == MessageKind.OrdinanceCreate &&
                            messageSettings.AllowFastCloseOrdinanceCreate.Value) ||
                        (message.MessageKind == MessageKind.OrdinanceAllClose &&
                            messageSettings.AllowFastCloseOrdinanceAllClose.Value) ||
                        (message.MessageKind == MessageKind.IncedentCreate &&
                            messageSettings.AllowFastCloseIncedentCreate.Value) ||
                        (message.MessageKind == MessageKind.ChallengeRework &&
                            messageSettings.AllowFastCloseChallengeRework.Value) ||
                        (message.MessageKind == MessageKind.ChallengeReject &&
                            messageSettings.AllowFastCloseChallengeReject.Value) ||
                        (message.MessageKind == MessageKind.ChallengeInWorkFromReject &&
                            messageSettings.AllowFastCloseChallengeInWorkFromReject.Value)
                        )
                    {
                        message.Delivered = true;
                        message.Save();
                        objectSpace.CommitChanges();

                    }
                }
            }
        }

        private void AlertControlButtonClick(object sender, AlertButtonClickEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();
            var collection = new CollectionSource(objectSpace, typeof(Message));
            collection.SetCriteria("OnlyCurrentUserUnreadMessages", @"not Delivered");
            _objectSpace = objectSpace;
            _collection = collection;
            collection.CollectionReloaded += ApplyFilter;
            collection.Reload();            
            objectSpace.ObjectReloaded += ApplyFilter;
            ApplyFilter(null, null);            
            var lw = Application.CreateListView("Message_ListView", collection, true);
            _objectSpace = lw.ObjectSpace;
            lw.ObjectSpace.Reloaded += ApplyFilter;
            var showViewParameters = new ShowViewParameters
            {
                CreatedView = lw,
                Context = TemplateContext.View,
                TargetWindow = TargetWindow.Default                
            };            
            var showViewSource = new ShowViewSource(Application.MainWindow, null);
            Application.ShowViewStrategy.ShowView(showViewParameters, showViewSource);
        }

        private void ApplyFilter(object sender, EventArgs e)
        {
            _objectSpace.ApplyFilter(_collection.Collection,
                CriteriaOperator.Or(
                    CriteriaOperator.And(
                        CriteriaOperator.Parse("ReceiverType = ?", "Роль"),
                        CriteriaOperator.Parse("MessageRoleValidationOperator(Receiver)")),
                    CriteriaOperator.And(
                        CriteriaOperator.Parse("ReceiverType = ?", "Пользователь"),
                        CriteriaOperator.Parse("Receiver = ?", SecuritySystem.CurrentUserName))
                    ));   
        }

        /// <summary>Создаем и запускаем таймер
        /// </summary>
        protected virtual void InitAlertTimerCore()
        {
            AlertTimer = new Timer();
            AlertTimer.Tick += TimerTick;
            AlertTimer.Interval = 30000;
            AlertTimer.Start();
        }

        /// <summary>Тик таймера, проверяем наличие непрочитанных уведомлений 
        /// адресованных текущему пользователю или одной из его ролей и выводим их
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerTick(object sender, EventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace();
            //MessageTriggerType == MessageTriggerType.WorkJournalRequest
            var newMessages = objectSpace.GetObjects<Message>(
                CriteriaOperator.Parse(@"not Delivered or MessageTriggerType = ?", 
                MessageTriggerType.WorkJournalRequest));
            newMessages = newMessages.Where(el => !el.Delivered).ToList();
            var roles = ((SecuritySystemUser)SecuritySystem.CurrentUser).Roles
                .Select(role => role.Name.ToLower()).ToList();                        
            newMessages = newMessages.Where(el =>
                ((el.ReceiverType == @"Роль")
                //03.04.15 обработка комплексных ролей 
                //&& (roles.Contains(el.Receiver)
                 && el.Receiver.ToLower().Split(new[] { "&&" }, StringSplitOptions.RemoveEmptyEntries)
                    .All(el2 => roles.Contains(el2.Trim())))
                 ||
                 ((el.ReceiverType == @"Пользователь")
                 && (SecuritySystem.CurrentUserName == el.Receiver))
                 ).ToList();
            // Получить настройки текущего пользователя для воспроизведения звука при всплывании уведомления
            var slice = SettingsAccessor.Instance.GetSlice(SecuritySystem.CurrentUser);
            var messageSettings = SettingsAccessor.Instance.GetRootSettings(slice).NotificationSystemSettingsEx().NotificationSystem.PopupMessages;  
            bool playSound = messageSettings.NotificationSound.PlayNotificationSound.Value;
            string pathToSoundFile = messageSettings.NotificationSound.PathToSoundFile.Value;            
            // Найти и загрузить файл для воспроизведения
            bool fileNotFound = false;
            if (playSound)
            {
                if (pathToSoundFile != string.Empty)
                {
                    if (File.Exists(pathToSoundFile))
                    {
                        if (_player.SoundLocation != pathToSoundFile)
                        {
                            _player.SoundLocation = pathToSoundFile;
                            _player.LoadTimeout = 5000;
                            _player.LoadAsync();
                        }
                    }
                    else
                        fileNotFound = true;
                }
                else
                    fileNotFound = true;
            }
            var i = 0;
            foreach (var message in newMessages)
            {
                i++;
                bool isExists = false;
                foreach (var alertForm in AlertControl.AlertFormList)
                {
                    if (ReferenceEquals(objectSpace.GetObject(alertForm.Info.Tag), message))
                    {
                        alertForm.AlertInfo.Caption = "(" + i + "/" + newMessages.Count + ") " + message.Theme;
                        isExists = true;
                        break;
                    }
                }
                foreach (var alertForm in AlertControl.PostponedFormList)
                {
                    if (ReferenceEquals(objectSpace.GetObject(alertForm.Info.Tag), message))
                    {
                        alertForm.AlertInfo.Caption = "(" + i + "/" + newMessages.Count + ") " + message.Theme;
                        isExists = true;
                        break;
                    }
                }
                if (isExists) continue;
                var alertInfo = new AlertInfo(
                        "(" + i + "/" + newMessages.Count + ") " + message.Theme,
                        (message.Content == null || message.Content.Trim() == String.Empty
                        ? "Нет описания" : message.Content),
                        message.Sender,// + Environment.NewLine + "Еще уведомлений: " + (newMessages.Count - 1),
                    //ImageLoader.Instance.GetImageInfo(@"BO_Attention").Image,
                        Resources.exclamation29,
                        message);
                AlertControl.Show((Form)Application.MainWindow.Template, alertInfo);
                // Проиграть звук
                if (playSound)
                {
                    try
                    {
                        if (!fileNotFound)
                            _player.Play();
                        else
                            SystemSounds.Exclamation.Play();
                    }
                    catch
                    {                        
                    }
                }
            }
            Messages = newMessages;
        }

        /// <summary>Обработчик нажатия на уведомление, помечаем сообщение как прочитанное и
        /// выводим его или связанный с ним объект в новой кладке или форме
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void AlertControlAlertClick(object sender, AlertClickEventArgs e)
        {
            #region Описание
            // Возможны следующие варианты поведения:
            //
            // 1) если в Сообщении указан ИД прикрепленного объекта, и тип объекта содержится 
            // в настройке приложения \Особое поведение для объектов\, 
            // то при щелчке на уведомлении откроется DetailView прикрепленного объекта
            //
            // 2) если ИД не указан и тип объекта содержится в настройке, 
            // то откроется ListView, используемое по умолчанию для типа прикрепленного объекта
            //
            // 3) если ИД указан, и тип объекта не содержится в настройке, 
            // то откроется DetailView самого Сообщения
            //
            // 4) если ИД не указан, и тип объекта не содержится в настройке,
            // то откроется DetailView самого Сообщения            
            #endregion

            // Получить экземпляр Сообщения, связанного с Уведомлением
            if (Application == null)
            {
                //если отрываем модель например то уведолмения еще весят но application = null                
                OnDeactivated();
                e.AlertForm.Close();
                return;
            }
            var objectSpace = Application.CreateObjectSpace();
            var message = objectSpace.GetObject(e.Info.Tag) as Message;
            if (message != null)
            {
                // Извлечь XPO-тип прикрепленного объекта из Сообщения
                XPObjectType xpObjType = message.AttachedObjectType;
                if (xpObjType != null)
                {
                    #region Описание настроек
                    // Получить настройки приложения из узла \Система уведомлений\Всплывающие сообщения\Особое поведение для объектов\
                    // для текущего пользователя 
                    // и поместить их значения в список. Значениями настроек являются наименования типов объектов (System.Type).
                    // Всего в данный момент данном узле 10 настроек, т.е. можно задать особое поведение для 10 типов объектов. 
                    // Суть: если какой-либо тип объекта указан в данной группе настроек, то при щелчке на всплывающее окно уведомления
                    // может быть показано View этого прикрепленного к сообщению объекта, а не View самого сообщения. 
                    // В противном случае может быть открыто View сообщения. 
                    #endregion
                    var slice = SettingsAccessor.Instance.GetSlice(SecuritySystem.CurrentUser);
                    var messageSettings = SettingsAccessor.Instance.GetRootSettings(slice).NotificationSystemSettingsEx().NotificationSystem.PopupMessages;
                    var excludeObjTypes = messageSettings.ExcludeObjectTypes;                      
                    #region Чтение настроек

                    var listoftypes = new List<Type>();

                    SettingsExTypeStorage settingsPersistent01 = excludeObjTypes.TypeStorageValue01.Value;
                    if (settingsPersistent01 != null) listoftypes.Add(settingsPersistent01.DataType);

                    SettingsExTypeStorage settingsPersistent02 = excludeObjTypes.TypeStorageValue02.Value;
                    if (settingsPersistent02 != null) listoftypes.Add(settingsPersistent02.DataType);

                    SettingsExTypeStorage settingsPersistent03 = excludeObjTypes.TypeStorageValue03.Value;
                    if (settingsPersistent03 != null) listoftypes.Add(settingsPersistent03.DataType);

                    SettingsExTypeStorage settingsPersistent04 = excludeObjTypes.TypeStorageValue04.Value;
                    if (settingsPersistent04 != null) listoftypes.Add(settingsPersistent04.DataType);

                    SettingsExTypeStorage settingsPersistent05 = excludeObjTypes.TypeStorageValue05.Value;
                    if (settingsPersistent05 != null) listoftypes.Add(settingsPersistent05.DataType);

                    SettingsExTypeStorage settingsPersistent06 = excludeObjTypes.TypeStorageValue06.Value;
                    if (settingsPersistent06 != null) listoftypes.Add(settingsPersistent06.DataType);

                    SettingsExTypeStorage settingsPersistent07 = excludeObjTypes.TypeStorageValue07.Value;
                    if (settingsPersistent07 != null) listoftypes.Add(settingsPersistent07.DataType);

                    SettingsExTypeStorage settingsPersistent08 = excludeObjTypes.TypeStorageValue08.Value;
                    if (settingsPersistent08 != null) listoftypes.Add(settingsPersistent08.DataType);

                    SettingsExTypeStorage settingsPersistent09 = excludeObjTypes.TypeStorageValue09.Value;
                    if (settingsPersistent09 != null) listoftypes.Add(settingsPersistent09.DataType);

                    SettingsExTypeStorage settingsPersistent10 = excludeObjTypes.TypeStorageValue10.Value;
                    if (settingsPersistent10 != null) listoftypes.Add(settingsPersistent10.DataType);
                    #endregion
                    
                    ShowViewParameters showViewParameters = null;
                    // Получить тип прикрепленного объекта из Сообщения
                    Type attachedObjType = xpObjType.SystemType;
                    // Проверить, указан ли идентификатор прикрепленного объекта.
                    if (message.AttachedObjectOid != null)
                    {
                        // Идентификатор вложенного объекта указан.
                        int keyIntValue = 0;
                        object attObject = null;
                        // Получить тип ключевого поля вложенного объекта. Либо int, либо Guid.
                        Type attObjKeyType = objectSpace.GetKeyPropertyType(attachedObjType);    
                           
                        // Проверка: содержится ли тип вложенного объекта в настройках
                        if (listoftypes.Contains(attachedObjType))
                        {
                            // Содержится. Получить вложенный объект для показа.
                            // Привести значение идентификатора объекта из строки либо к int, либо к Guid.
                            if (attObjKeyType == typeof(int))
                            {
                                try
                                {
                                    keyIntValue = Convert.ToInt32(message.AttachedObjectOid);
                                }
                                catch (Exception except)
                                {
                                    Tracing.Tracer.LogError(except);
                                    MessageBox.Show(except.Message,
                                        @"Ошибка",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                                }

                                attObject = objectSpace.GetObjectByKey(attachedObjType, keyIntValue);
                            }
                            if (attObjKeyType == typeof(Guid))
                            {
                                Guid keyGuidValue;
                                if (Guid.TryParseExact(message.AttachedObjectOid, "D", out keyGuidValue))
                                    attObject = objectSpace.GetObjectByKey(attachedObjType, keyGuidValue);
                            }
                        }
                        else
                        {
                            // Не содержится. Использовать для показа само Сообщение  
                            attObject = message;
                        }

                        // Создать DetailView для показа объекта
                        if (attObject != null)
                        {
                            var detailView = Application.CreateDetailView(objectSpace, attObject, true);
                            showViewParameters = new ShowViewParameters
                            {
                                CreatedView = detailView,
                                Context = TemplateContext.View,
                                TargetWindow = TargetWindow.Default,
                                CreateAllControllers = true
                            };
                        } 
                    }
                    else
                    {
                        // Идентификатор вложенного объекта не указан.  
                        // Проверка: содержится ли тип вложенного объекта в настройках
                        if (listoftypes.Contains(attachedObjType))
                        {
                            // Содержится. Получить тип вложенного объекта для показа.
                            // Создать ListView для показа списка объектов

                            var listView = Application.CreateListView(objectSpace, attachedObjType, true);
                            showViewParameters = new ShowViewParameters
                            {
                                CreatedView = listView,
                                Context = TemplateContext.View,
                                TargetWindow = TargetWindow.Default,
                                CreateAllControllers = true,                                
                            };
                        }
                        else
                        {
                            // Не содержится. Создать DetailView Сообщения для показа.                        
                            var detailView = Application.CreateDetailView(objectSpace, message, true);
                            showViewParameters = new ShowViewParameters
                            {
                                CreatedView = detailView,
                                Context = TemplateContext.View,
                                TargetWindow = TargetWindow.Default,
                                CreateAllControllers = true
                            };
                        }
                    }
                    // Показать View объекта                    
                    Application.ShowViewStrategy.ShowView(
                        showViewParameters, 
                        new ShowViewSource(Application.MainWindow, null));
                }
                // Пометить Сообщение как прочитанное
                UnitOfWork uow = new UnitOfWork();
                Message mes = uow.GetObjectByKey<Message>(message.Oid);
                mes.Delivered = true;
                uow.CommitChanges();
            }

            //objectSpace.CommitChanges();
            // Закрыть всплывающее окно
            e.AlertForm.Close();
        }
    }
}
