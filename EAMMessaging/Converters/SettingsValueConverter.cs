﻿using DevExpress.ExpressApp;
using DevExpress.Xpo;
using Galaktika.Core.Module.Converters;
using System;
using System.ComponentModel;
using System.Linq;
using Xafari.BC.Settings;

namespace PIR.EAM.Messaging.Converters
{
    /// <summary>Конвертер выполняет замену значений некоторых настроек в хранилище настроек
    /// </summary>
    [Description("Конвертация настроек системы уведомлений")]
    [Number(500)]
    [NonPersistent]
    [RequiredVersion(0, 0, 0, 0)]
    [MultipleExecute(false)]
    [BreakConvertWhenFailed(false)]
    public class SettingsValueConverter : BaseConverter
    {
        public SettingsValueConverter(IObjectSpace objectSpace, Version currentDBVersion, Type moduleType) :
            base(objectSpace, currentDBVersion, moduleType) { }

        protected override void ExecuteCore()
        {
            const string typeName = "IPLAST.Settings.BusinessObjects.SettingsExTypeStorage";
            this.TraceInformation("Запуск конвертера");
            this.TraceInformation("Получить настройки из объекта 'SettingsValueSliceStorage'");
            UnitOfWork uow = new UnitOfWork();
            XPQuery<SettingsValueSliceStorage> setStorageQuery = new XPQuery<SettingsValueSliceStorage>(uow);
            var valueFields = from valueField in setStorageQuery
                              where (valueField.Values != null &&
                                     valueField.Values.Trim() != null &&
                                     valueField.Values.Trim() != "")
                              select valueField;

            foreach (var item in valueFields)
            {
                this.TraceInformation("Загрузить настройку как текст");
                string val = item.Values;
                if (val.Contains(typeName))
                {
                    val = val.Replace(typeName, "PIR.EAM.Services.Settings.BusinessObjects.SettingsExTypeStorage");
                    item.Values = val;
                    item.Save();
                    this.TraceInformation("Настройка заменена");
                }
                else
                {
                    this.TraceInformation("Настройка не содержит требуемой строки. Пропущено.");
                }                
            }
            uow.CommitChanges();
            this.TraceInformation("Работа завершена.");

        }
    }
}
