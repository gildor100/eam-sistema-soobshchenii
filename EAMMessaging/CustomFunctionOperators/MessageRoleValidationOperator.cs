﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security.Strategy;
using System;
using System.Linq;

namespace PIR.EAM.Messaging.CustomFunctionOperators
{

    public class MessageRoleValidationOperator : ICustomFunctionOperator
    {
        public string Name
        {
            get { return "MessageRoleValidationOperator"; }
        }
        /*
        //serverside fomatting, что бы функция работала на сервере нужно определить этот метод
        //и возвращать строку передаваемую в запросе
        //но поскольку строку вида роль&&роль не разбить без создания функции в БД
        //что можно сделать в апдейтере например
        //обойдемся фильтром на клиенте 
        public string Format(Type providerType, params string[] operands)
        {
            if (!operands.Any()) return "";
            var rawString = operands.First();
            var user = SecuritySystem.CurrentUser as SecuritySystemUser;
            if (rawString == null || user == null) return "";
            var roles = user.Roles.Select(el => el.Name.ToLower());
            return rawString.ToLower().Split(new[] { "&&" }, StringSplitOptions.RemoveEmptyEntries)
                .All(el2 =>  el2.Trim() roles.Contains(el2.Trim()));
            //return string.Format("datepart(\"m\", {0})", operands[0]);
        }
        //*/
        public object Evaluate(params object[] operands)
        {
            //operands[0] - "role&&role"
            if (!operands.Any()) return false;
            var rawString = operands.First() as String;
            var user = SecuritySystem.CurrentUser as SecuritySystemUser;
            if (rawString == null || user == null) return false;
            var roles = user.Roles.Select(el => el.Name.ToLower()).ToList();
            return rawString.ToLower().Split(new[] { "&&" }, StringSplitOptions.RemoveEmptyEntries)
                .All(el2 => roles.Contains(el2.Trim()));
            //return ((Employee) SecuritySystem.CurrentUser).Company.Oid;
        }

        public Type ResultType(params Type[] operands)
        {
            return typeof (bool);
        }

        static MessageRoleValidationOperator()
        {
            var instance = new MessageRoleValidationOperator();
            if (CriteriaOperator.GetCustomFunction(instance.Name) == null)
            {
                CriteriaOperator.RegisterCustomFunction(instance);
            }
        }

        public static void Register()
        {
        }
    }
}
