﻿namespace PIR.EAM.Messaging.Enums
{
    public enum MessageTriggerType
    {
        Default,
        WorkJournalRequest
    }
}
