﻿namespace PIR.EAM.Messaging.Enums
{
    public enum MessageKind
    {
        Other,
        IncedentCreate,
        ChallengeReject,
        ChallengeRework,
        ChallengeInWorkFromReject,
        OrdinanceCreate,
        OrdinanceAllClose,        
    }
}
