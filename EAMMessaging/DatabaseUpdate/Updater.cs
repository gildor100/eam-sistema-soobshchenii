﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Xpo;
using PIR.EAM.Messaging.BusinessObjects;
using System;

namespace PIR.EAM.Messaging.DatabaseUpdate
{
    public class Updater : ModuleUpdater
    {
        public Updater(IObjectSpace objectSpace, Version currentDbVersion) :
            base(objectSpace, currentDbVersion)
        {
        }

        public override void UpdateDatabaseBeforeUpdateSchema()
        {
            base.UpdateDatabaseBeforeUpdateSchema();
            UpdateXPObjectType("EAMAdditionalControllers.BusinessObjects.Message",
                typeof(Message).ToString(),
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                );
        }

        public override void UpdateDatabaseAfterUpdateSchema()
        {
            base.UpdateDatabaseAfterUpdateSchema();
            var objectSpace = ObjectSpace as XPObjectSpace;
            if (objectSpace == null) return;
            const string updateCommand = "update SecuritySystemTypePermissionsObject set TargetType = '{0}'" +
                                         " where TargetType = '{1}'";
            objectSpace.Session.ExecuteNonQuery(String.Format(updateCommand,
                typeof(Message),
                "EAMAdditionalControllers.BusinessObjects.Message"));
            objectSpace.CommitChanges();
        }
    }
}
