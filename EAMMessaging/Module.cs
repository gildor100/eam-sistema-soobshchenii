using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using PIR.EAM.Messaging.CustomFunctionOperators;
using PIR.EAM.Messaging.Settings;
using Xafari.BC.Settings;

namespace PIR.EAM.Messaging
{
    public sealed partial class EAMMessagingModule : ModuleBase
    {
        public EAMMessagingModule()
        {              
            InitializeComponent();
            MessageRoleValidationOperator.Register();
        }

        public override void CustomizeTypesInfo(DevExpress.ExpressApp.DC.ITypesInfo typesInfo)
        {
            base.CustomizeTypesInfo(typesInfo);
        }

        /// <summary>� ������ ������ ����������� ���������� ���� ������ �������� ����������, 
        /// ������������� � Xafari.BC.Settings, �������������� �����.
        /// </summary>
        /// <param name="extenders"></param>
        public override void ExtendModelInterfaces(ModelInterfaceExtenders extenders)
        {
            base.ExtendModelInterfaces(extenders);
            extenders.Add<IModelBCSettings, INotificationSystemExtendedSettings>();
        }
    }
}
