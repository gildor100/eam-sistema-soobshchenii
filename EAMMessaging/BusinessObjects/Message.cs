﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using Galaktika.Core.Module;
using Galaktika.EAM.Module.Работы;
using PIR.EAM.Messaging.Enums;
using PIR.EAM.Services.Services;
using System;
using System.ComponentModel;
using System.Linq;

namespace PIR.EAM.Messaging.BusinessObjects
{
    /// <summary>Сообщение используемое для уведомления пользователей
    /// За обработку сообщений отвечает контроллер MessageController
    /// </summary>
    [Persistent]
    [System.ComponentModel.DisplayName(@"Сообщение")]
    [ModelDefault("Caption", "Сообщение")]
    public sealed class Message : XPObject
    {
        [DevExpress.Xpo.DisplayName("Отправитель")]
        [ModelDefault("AllowEdit", "false")]
        public string Sender { get; set; }

        /// <summary>
        /// 03.04.15 Возникла необходимость укзания подразделения получателя уведомления
        /// например 2 дежурных ОГМ в цех 1 и цех 2
        /// как вариант можно было добавить доп поле
        /// но лучше сделаем комплексное поле получатель
        /// разделитель получаетелей будет "&&" - и
        /// пока нет необходимости в более сложных операторах вроде "или" или "не"
        /// в принципе можно реализовать через CriteriaOperator
        /// </summary>
        [DevExpress.Xpo.DisplayName("Получатель")]
        [ModelDefault("AllowEdit", "false")]
        public string Receiver { get; set; }

        [DevExpress.Xpo.DisplayName("Тип получателя")]
        [ModelDefault("AllowEdit", "false")]
        public string ReceiverType { get; set; }

        private bool _delivered;
        [DevExpress.Xpo.DisplayName("Доставлено")]
        [ModelDefault("AllowEdit", "false")]
        [VisibleInDetailView(false)]
        public bool Delivered {
            get
            {
                var tmp = _delivered;
                //Елси уведомление - Запрос включения в график то особая обработка
                if (MessageTriggerType == MessageTriggerType.WorkJournalRequest && !IsLoading && !IsSaving)
                {
                    var journal = Session.FindObject<ЖурналРабот>(CriteriaOperator
                        .Parse("Oid = ?", AttachedObjectOid));
                    if (journal != null && 
                        journal.ДокументОснование != null && 
                        journal.ДокументОснование.График == null)
                    {
                        //Если не включен в график и не уничтожен то не доставлено
                        tmp = false;                                                
                    }
                    else
                    {
                        tmp = true;
                        //возвращем триггер что бы уведомление не проверялось и вываливалось всю жизнь
                        MessageTriggerType = MessageTriggerType.Default;
                        SetPropertyValue("Delivered", ref _delivered, true);
                    }
                }
                return tmp;
            }
            set { SetPropertyValue("Delivered", ref _delivered, value); }
        }

        [DevExpress.Xpo.DisplayName("Дата создания")]
        [ModelDefault("DisplayFormat","dd.MM.yyyy HH:mm:ss")]
        [ModelDefault("AllowEdit", "false")]
        public DateTime Date { get; set; }

        private string _theme;
        [DevExpress.Xpo.DisplayName("Тема")]
        [ModelDefault("AllowEdit", "false")]
        [Size(300)]
        public string Theme
        {
            get { return _theme; }
            set
            {
                // Если длина строки больше длины поля, обрезать строку.
                // В противном случае при программном заполнении этого свойства 
                // возникнет исключение sql, сообщающее о превышении длины поля
                int maxLength = ClassInfo.GetMember("Theme").MappingFieldSize;                                
                if (value != null && value.Length > maxLength)
                    SetPropertyValue("Theme", ref _theme, value.Substring(0, maxLength));
                else
                    SetPropertyValue("Theme", ref _theme, value);
            }
        }

        private string _content;
        [DevExpress.Xpo.DisplayName("Сообщение")]
        [ModelDefault("AllowEdit", "false")]
        [ModelDefault("RowCount", "3")]
        [Size(2000)]
        public string Content 
        {
            get { return _content; } 
            set 
            {
                // Если длина строки больше длины поля, обрезать строку.
                // В противном случае при программном заполнении этого свойства 
                // возникнет исключение sql, сообщающее о превышении длины поля
                int maxLength = ClassInfo.GetMember("Content").MappingFieldSize;
                if (value != null && value.Length > maxLength)
                    SetPropertyValue("Theme", ref _content, value.Substring(0, maxLength));
                else
                    SetPropertyValue("Theme", ref _content, value);
            } 
        }

        [Browsable(false)]
        [Size(36)]
        public string AttachedObjectOid { get; set; }

        [Browsable(false)]
        [NoForeignKey]
        public XPObjectType AttachedObjectType { get; set; }

        //Отмечать как доставленное при закрытии уведомления
        [Browsable(false)] 
        public bool DeliveredOnClose { get; set; }

        /// <summary>Конструктор, при создании сообщение помечается как недоставленное
        /// и созданное в настоящий момент времени
        /// </summary>
        /// <param name="session">Сессия</param>
        public Message(Session session) : base(session)
        {
            Delivered = false;
            Date = DateTime.Now;
            
        }

        [Browsable(false)]
        public MessageTriggerType MessageTriggerType { get; set; }

        [Browsable(false)]
        public MessageKind MessageKind { get; set; }

        /// <summary>Пометить как прочитанные все непрочитанные сообщения, в которые вложен объект
        /// </summary>
        /// <param name="objectSpace"></param>
        /// <param name="obj"></param>
        public static void MarkMessagesAsDelivered<T>(IObjectSpace objectSpace, T obj) where T : БазовыйОбъект
        {
            if (obj == null) return;
            var arrobj = new object[] { obj.Oid, ObjectTypeService.GetXPObjectTypeOid(obj) };
            var critOper = CriteriaOperator.Parse(
                "AttachedObjectOid = ? And AttachedObjectType = ? And Delivered = false", arrobj);
            if (objectSpace != null)
            {
                var messageList = objectSpace.GetObjects<Message>(critOper);
                if (messageList.Any())
                {
                    foreach (var msg in messageList.Where(msg => !msg.Delivered))
                    {
                        msg.Delivered = true;
                        msg.Save();
                    }
                }
            }
        }
    }
}
